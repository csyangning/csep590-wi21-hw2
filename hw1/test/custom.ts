import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("put description of test 1 here", () => {

        // put the body of the test here
        const e = new EngExp().match("a").asRegExp();

        const result = e.exec("a");

        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.
        expect(e.test("a")).to.be.true;
        expect(result[0]).to.be.equal("a");
    });

    // You should modify the above test so it's interesting, and provide
    // at least 4 more nontrivial tests.

});
