import {expect} from "chai";

import * as q from "../src/q";
import * as queries from "../src/queries";
import {crimeData} from "./data";
const Q = q.Q;
const rawData = crimeData.data;

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

describe("custom", () => {

    it("Test filer and count to be zero",  () => {
        const test_data = [[1, "a"], [2, "b"], [3, "c"]];
        const query_filter_count = Q.filter((x) => x[1] == "x").count();

        expect(query_filter_count.execute(test_data)[0]).to.be.equal(0);
    });

    it("Test join on two tables", () => {
        const data = [{f1: 1, f2: 2}, {f1: 3, f2: 4}];
        const modified_data = Q.apply((x) => {return {f1: x.f1, f3: 10};});
        const join_query = Q.join(modified_data, "f1");

        const out = join_query.execute(data);
        expect(out).to.be.an("Array");
        expect(out).to.have.length(2);
        expect(out[0]).to.deep.equal({f1:1, f2:2, f3: 10});
    });

    it("Expects functional join to be equal to field joins", () => {

        const data = queries.cleanupQuery.execute(rawData);
        const out1 = queries.crimeJoinQuery1.execute(data);
        const out2 = queries.crimeJoinQuery2.execute(data);

        expect(out1).to.deep.equal(out2);
    });

    it("Expects join then filter to be equal in length to call chaining filter and join", () => {

        const data = queries.cleanupQuery.execute(rawData);
        const out1 = queries.crimeJoinQuery3.execute(data);
        const out2 = queries.dateTimeQuery2.count().execute(out1);

        const out3 = queries.crimeJoinQuery2.count().execute(data);

        expect(out2).to.deep.equal(out3);
    });

    it("Expects outputs from joins on fields and hashjoins to be equal in length", () => {

        const data = queries.cleanupQuery.execute(rawData);
        const out1 = queries.crimeJoinQuery3.count().execute(data);
        const out3 = queries.crimeJoinQuery4.count().execute(data);

        expect(out1).to.deep.equal(out3);
    });
});
