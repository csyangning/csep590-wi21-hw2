/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

function isObject(val: any): boolean {
    if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
}

 // DeepClone of an array
function CloneArray(data: any[]): any[] {
    return JSON.parse(JSON.stringify(data));
}

// This class represents all AST Nodes.
export class ASTNode {
    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(callback: (datum: any) => any): ASTNode {
        return new ThenNode(this, new ApplyNode(callback));
    }

    count(): ASTNode {
        return new ThenNode(this, new CountNode());
    }

    product(query: ASTNode): ASTNode {
        return new CartesianProductNode(this, query);
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        if (typeof(relation) === "string") {
            // TODO: fix this on Part3 question 4.
            return new JoinNode(this, query, relation, null);
        } else {
            return new JoinNode(this, query, null, relation);
        }
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return CloneArray(data);
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        const cloned_array = CloneArray(data);
        return cloned_array.filter(this.predicate);
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        const cloned_data = CloneArray(data);
        const first_output = this.first.execute(cloned_data);
        return this.second.execute(first_output);
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    map_callback: (datum: any) => any;

    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.map_callback = callback;
    }

    execute(data: any[]): any {
        const cloned_array = CloneArray(data);
        return cloned_array.map(this.map_callback);
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    execute(data: any[]): any {
        const cloned_array = CloneArray(data);
        return [cloned_array.length];
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof FilterNode) {
        // case for henNode(FilterNode(f), FilterNode(g))
        if (this.first instanceof FilterNode) {
            return new FilterNode(this.first.predicate && this.second.predicate);
        }
        // case for ThenNode(ThenNode(x, FilterNode(f)), FilterNode(g))
        if (this.first instanceof ThenNode && this.first.second instanceof FilterNode) {
            return new ThenNode(
                this.first.first, 
                new FilterNode(this.first.second.predicate && this.second.predicate));
        }
    }

    return null;
});


//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;
    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        let count = 0;

        data.forEach(d => {
             if (this.predicate(d)) {
                 count++;
             }
            });

        return [count];
    }
}

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof CountNode) {
        if (this.first instanceof ThenNode && this.first.second instanceof FilterNode) {
            return new ThenNode(
                this.first.first,
                new CountIfNode(this.first.second.predicate)
            );
        }

        if (this.first instanceof FilterNode) {
            return new CountIfNode(this.first.predicate);
        }
    }
    return null;
});

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any {
        const left_output = CloneArray(this.left.execute(data));
        const right_output = CloneArray(this.right.execute(data));
        let output = [];
        
        for (const left_data of left_output) {
            for (const right_data of right_output) {
                output.push({left: left_data, right: right_data});
            }
        }

        return output;
    }

    optimize(): ASTNode {
        return new CartesianProductNode(this.left.optimize(), this.right.optimize());
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    join_function: (l: any, r: any) => any;
    join_field: string;

    constructor(l: ASTNode, r: ASTNode, s ?: string, f ?: (l: any, r: any) => any) { // you may want to add some proper arguments to this constructor
        super("Join");
        this.left = l;
        this.right = r;
        this.join_function = f;
        this.join_field = s;
    }
    
    execute(data: any[]): any {
        if (this.join_function) {
            return this.execute_function(data);
        }

        return this.execute_field(data)

    }

    execute_field(data: any[]): any {
        const product_node = new CartesianProductNode(this.left, this.right);
        const product_result = CloneArray(product_node.execute(data));
        let output = [];
        for (const r of product_result) {
            if ((this.join_field in r.left && this.join_field in r.right)
                && (r.left[this.join_field] == r.right[this.join_field])) {

                output.push(Object.assign(r.left, r.right));

            }
        }
        
        return output;
    }

    execute_function(data: any[]): any {
        const product_node = new CartesianProductNode(this.left, this.right);
        const product_result = CloneArray(product_node.execute(data));
        let output = [];

        for(const r of product_result) {
            if (this.join_function(r.left, r.right)) {
                output.push(Object.assign(r.left, r.right));
            }
        }
        
        return output;
    }
    
    optimize(): ASTNode {
        if (this.join_field) {
            return new HashJoinNode(this.join_field, this.left.optimize(), this.right.optimize());
        }
        return new JoinNode(this.left.optimize(), this.right.optimize(), this.join_field, this.join_function);
    }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    
    join_field: string;
    constructor(s : string, l : ASTNode, r : ASTNode) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        this.left = l;
        this.right = r;
        this.join_field = s;
        //throw new Error("Unimplemented AST node " + this.type);
    }

    execute(data : any[]): any {
        let arr_one_hash = {}
        let arr_two_hash = {}
        const left_output = CloneArray(this.left.execute(data));
        const right_output = CloneArray(this.right.execute(data));
        for (const elem of left_output) {
            if (this.join_field in elem) {
                if (elem[this.join_field] in arr_one_hash) {
                    arr_one_hash[elem[this.join_field]].push(elem);
                } else {
                    arr_one_hash[elem[this.join_field]] = [elem];
                }
            }
        }

        for (const elem of right_output) {
            if (this.join_field in elem) {
                if (elem[this.join_field] in arr_two_hash) {
                    arr_two_hash[elem[this.join_field]].push(elem);
                } else {
                    arr_two_hash[elem[this.join_field]] = [elem];
                }
            }
        }
        let output = []
        for (let key in arr_one_hash) {
            let rightValue = arr_one_hash[key];
            if (key in arr_two_hash) {
                let leftValue = arr_two_hash[key];
                for (const right_elem  of rightValue) {
                    for (const left_elem of leftValue) {
                        output.push(Object.assign(left_elem, right_elem));

                    }
                }
            }
        }
        return output;

    }
}
