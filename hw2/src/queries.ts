import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.ThenNode(
  new q.IdNode(), 
  new q.FilterNode((x) => (x[2] as string).match(/THEFT/) != null)
  );
export const autoTheftsQuery = new q.ThenNode(
  new q.IdNode(), 
  new q.FilterNode((x) => (x[3] as string) == "MOTOR VEHICLE THEFT")
  );

//// 1.4 clean the data

function CleanMap(x: any[]): any {
  return {description: x[2], category: x[3], area: x[4], date: x[5]};
}

export const cleanupQuery = new q.ThenNode(
  new q.IdNode(),
  new q.ApplyNode(CleanMap),
  );

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply(CleanMap);
export const theftsQuery2 = Q.filter((x) => x.description.match(/THEFT/) != null);
export const autoTheftsQuery2 = Q.filter((x) => x.category == "MOTOR VEHICLE THEFT");

//// 4 put your queries here (remember to export them for use in tests)
export const dateTimeQuery1 = Q.filter((x) => x.date.match(/20[0-9][0-9]-[0-9]{2}-[0-9]{2}T([0-9]{2}:){2}[0-9]{2}.[0-9]{3}/) != null);
export const dateTimeQuery2 = Q.filter((x) => x.date.match(/19[0-9][0-9]-[0-9]{2}-[0-9]{2}T([0-9]{2}:){2}[0-9]{2}.[0-9]{3}/) != null);
export const crimeJoinQuery1 = new q.JoinNode(dateTimeQuery2, new q.IdNode(), null, (l, r) => l.category === r.category);
export const crimeJoinQuery2 = new q.JoinNode(dateTimeQuery2, new q.IdNode(), "category", null);
export const crimeJoinQuery3 = new q.JoinNode(new q.IdNode(), new q.IdNode(), null, (l, r) => l.category === r.category);
export const crimeJoinQuery4 = new q.HashJoinNode("category", new q.IdNode(), new q.IdNode());